package test;

import org.junit.jupiter.api.Test;

import inscription.modele.Inscription;
import inscription.modele.InscriptionInvalideException;
import inscription.modele.InscriptionRequeteCreate;
import inscription.modele.InscriptionService;

import static org.junit.jupiter.api.Assertions.*;

public class RegisterTest {

	@Test
	public void testRegister() {
		InscriptionRequeteCreate requete = new InscriptionRequeteCreate();
		requete.setNom("Tutur");
		requete.setPrenom("Cham");
		requete.setApprobation(true);
		requete.setEmail("ffrf@tghy.fr");
		requete.setMotDePasse("midjeizyded");
		requete.setConfirmationMotDePasse("midjeizyded");
		requete.setClasse("31012020");
		InscriptionService service = new InscriptionService();
		Inscription inscription = new Inscription();
		try {
			inscription = service.inscrire(requete);
		}catch(InscriptionInvalideException e) {
			System.out.println(e);
		}
		
		assertEquals(requete.getNom(),inscription.getNom());
			
	}
	
	@Test
	public void testRegister2() {
		InscriptionRequeteCreate requete = new InscriptionRequeteCreate();
		requete.setNom("Tutur");
		requete.setPrenom("Cham");
		requete.setApprobation(true);
		requete.setEmail("ffrf@tghy.fr");
		requete.setMotDePasse("midjeizyded");
		requete.setConfirmationMotDePasse("midjeizyded");
		requete.setClasse("31012020");
		InscriptionService service = new InscriptionService();
		Inscription inscription = new Inscription();
		try {
			inscription = service.inscrire(requete);
		}catch(InscriptionInvalideException e) {
			System.out.println(e);
		}
		
		assertEquals(requete.getPrenom(),inscription.getPrenom());
			
	}
}
