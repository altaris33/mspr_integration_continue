package inscription.modele;

public class InscriptionService {
	
	public Inscription inscrire(InscriptionRequeteCreate requete) throws InscriptionInvalideException {
		
		InscriptionInvalideException ex = new InscriptionInvalideException();
		
		if(requete.getNom() == null || requete.getNom().isEmpty()) {
			ex.addMessage("nom", "Le champ nom doit être remplie !");
		}
		
		if(requete.getPrenom() == null || requete.getPrenom().isEmpty()) {
			ex.addMessage("prenom", "Le champ prénom doit être remplie !");
		}
		if(requete.getClasse() == null || requete.getClasse().isEmpty()) {
			ex.addMessage("classe", "Veuillez choisir une classe !");
		}
		if (requete.getEmail()== null || ! requete.getEmail().contains("@")) {
			ex.addMessage("email", "L'email est invalide !");
		}
		if (requete.getMotDePasse() == null || requete.getMotDePasse().length() < 8) {
			ex.addMessage("motDePasse", "Le mot de passe doit contenir au moins 8 caractères !");
		}
		if (!requete.getMotDePasse().equals(requete.getConfirmationMotDePasse())) {
			ex.addMessage("confirmationMotDePasse", "Les deux mots de passe ne sont pas identiques !");
		}
		if (!requete.isApprobation()) {
			ex.addMessage("approbation", "Vous devez accepter les conditions.");
		}
		if (ex.mustBeThrown()) {
			throw ex;
		}
		
		return new Inscription(requete.getNom(), requete.getPrenom(), "Annne", requete.getEmail(), requete.getMotDePasse());
	}
	
}
