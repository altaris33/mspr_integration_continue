package inscription.modele;

public class InscriptionRequeteCreate {
	
	private String nom;
	private String prenom;
	private String email;
	private String classe;
	
	private String motDePasse;
	private String confirmationMotDePasse;
	private boolean approbation;
	
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMotDePasse() {
		return motDePasse;
	}
	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}
	public String getConfirmationMotDePasse() {
		return confirmationMotDePasse;
	}
	public void setConfirmationMotDePasse(String confirmationMotDePasse) {
		this.confirmationMotDePasse = confirmationMotDePasse;
	}
	public boolean isApprobation() {
		return approbation;
	}
	public void setApprobation(boolean approbation) {
		this.approbation = approbation;
	}
	public String getClasse() {
		return classe;
	}
	public void setClasse(String parameter) {
		this.classe = parameter;
		
	}
	
	

}
