package inscription.modele;

import java.util.ArrayList;
import java.util.List;

public class AnneeClasse {
	
	private ArrayList<String> Classe ;
	
	public AnneeClasse(){
		this.Classe = new ArrayList<String>();
		Classe.add("Bachelor 1");
		Classe.add("Bachelor 2");
		Classe.add("Bachelor 3");
		Classe.add("Ingénieur niveau 4 ");
		Classe.add("Ingenieur niveau 5");
	}

	public ArrayList<String> getClasse() {
		return Classe;
	}

	public void setClasse(ArrayList<String> classe) {
		Classe = classe;
	}

}
