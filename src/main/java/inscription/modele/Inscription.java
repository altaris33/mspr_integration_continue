package inscription.modele;

import java.util.Date;

public class Inscription {
	private String nom;
	private String prenom;
	private String annee;	
	private String email;
	private String motDePasse;
	private Date date;
	
	public Inscription(String nom, String prenom, String annee, String email, String password) {
		this.nom = nom;
		this.prenom =prenom;
		this.annee = annee;
		this.email = email;
		this.motDePasse = password;
		this.date = new Date();
	}
	
	public Inscription() {
		
	}
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getAnnee() {
		return annee;
	}

	public void setAnnee(String annee) {
		this.annee = annee;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMotDePasse() {
		return motDePasse;
	}

	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
