package inscription.web;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import inscription.modele.AnneeClasse;
import inscription.modele.Inscription;
import inscription.modele.InscriptionInvalideException;
import inscription.modele.InscriptionRequeteCreate;
import inscription.modele.InscriptionService;

@WebServlet("/inscription")
public class InscriptionControleurServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static AnneeClasse Classe = new AnneeClasse();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		req.setAttribute("getClasse", Classe.getClasse());
		RequestDispatcher rd = getServletContext().getRequestDispatcher("/WEB-INF/jsp/inscription.jsp");
		
		rd.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		InscriptionRequeteCreate requete = new InscriptionRequeteCreate();
		
		requete.setNom(req.getParameter("nom"));
		requete.setPrenom(req.getParameter("prenom"));
		requete.setEmail(req.getParameter("email"));
		requete.setClasse(req.getParameter("classe"));
		requete.setMotDePasse(req.getParameter("motDePasse"));
		requete.setConfirmationMotDePasse(req.getParameter("confirmationMotDePasse"));
		requete.setApprobation(Boolean.valueOf(req.getParameter("approbation")));
		
		try {
			InscriptionService inscriptionService = new InscriptionService();

			Inscription inscription = inscriptionService.inscrire(requete);
			req.setAttribute("inscription", inscription);
			
			RequestDispatcher rd = getServletContext().getRequestDispatcher("/WEB-INF/jsp/validationInscription.jsp");
			rd.forward(req, resp);
		} catch (InscriptionInvalideException e) {
			
			req.setAttribute("errors", e.getMessages());
			req.setAttribute("getClasse", Classe.getClasse());
			
			RequestDispatcher rd = getServletContext().getRequestDispatcher("/WEB-INF/jsp/inscription.jsp");
			rd.forward(req, resp);
		}
	}
}
